<?php


class Question
{
    private $question;
    private $result;
    public function __construct($question,$result)
    {
        $this->question = $question;
        $this->result = $result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result .= $result;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question)
    {
        $this->question .= $question;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->question.$this->result."\n-----\n";
    }
}

?>